# (Dispersed) overleaf

Let's deploy overleaf using 1VM per service (which makes 1 docker container per VM per sevice since since we're using docker containers ...)

## How

We take the `sharelatex:3.0.1` and craft a dedicated image per service. in short
- `web = sharelatex:3.0.1 - chat - clsi - contacts ...`
- and the same procedure is repeated for all overleaf service.

## Deploy on your local machine

```bash
docker-compose up -d
```


## Deploy on VMs

First create the base image:
```bash
cd packer
packer build alpine-3.11.3-docker-x86_64.json
```

## Deploy with VMonG5k (aio)

```bash
python g5k.py
```


## Deploy with Tansiv (g5k)

```bash
python ts.py
```