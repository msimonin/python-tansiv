#!/bin/sh -eux

cat > /etc/apk/repositories << EOF; $(echo)
https://dl-cdn.alpinelinux.org/alpine/v$(cut -d'.' -f1,2 /etc/alpine-release)/main/
https://dl-cdn.alpinelinux.org/alpine/v$(cut -d'.' -f1,2 /etc/alpine-release)/community/
EOF


apk update
# the dep on netifaces is surprinsigly not included in cloud init ?!
apk add docker \
  cloud-init \
  py3-netifaces \
  git \
  gcc \
  python3-dev \
  musl-dev \
  libffi-dev \
  iproute2

# docker configuration
rc-update add docker boot
service docker start

# cloud-init config
rc-update add cloud-init default
rc-update add cloud-init-local default
rc-update add cloud-config default
rc-update add cloud-final default

# defer networking service after cloud-init
rc-update del networking boot
rc-update add networking default

# python config
python3 -m ensurepip
python3 -m pip install -U pip

# docker binding
python3 -m pip install docker

# sharelatex homemade client
python3 -m pip install git+https://gitlab.inria.fr/sed-rennes/sharelatex/python-sharelatex@inria-3.x

#echo "kernel.pax.softmode=1" > /etc/sysctl.d/01-docker.conf

# Prefetch all the required service
SERVICES="chat clsi contacts docstore documentupdater filestore notifications realtime spelling trackchanges web redis mongo"
for service in $SERVICES
do
  docker pull registry.gitlab.inria.fr/tansiv/python-tansiv/${service}-tansiv-overleaf:latest
done
