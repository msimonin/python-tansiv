from pathlib import Path
import time
from typing import List, Optional
from unittest.mock import DEFAULT

import enoslib as en

from ..api import background_shell

HERE = Path(__file__).parent.absolute()

DEFAULT_CONF = HERE / "redis.conf"


def redis_setup(
    redis_hosts: List[en.Host],
    server_port: int = 7000,
    config_file: Path  = DEFAULT_CONF,
    cluster_replicas: int = 1,
):
    """Setup a Redis cluster.

    There will be one cluster node per role in tansiv_roles.

    Args:
        redis_hosts: hosts on which a redis-server process will be configured and launched
        config_file: configuration file to use for all redis-server processes; better omit the port number and only use the server_port argument instead
        server_port: port number to reach the cluster nodes; must match the port number written in the configuration file if any
        cluster_replicas: number of replicas configured for each master node of the cluster (default: 1)
    """

    # 1. Use EnOSlib Provider p.copy() to copy config_file in each VM
    # 2. Build the command line that will be used for each redis-server process
    #    The command line should include the server_port using the --port argument or redis-server
    # 3. Use background_shell() to launch redis-server as a background process in each VM
    # 4. Build the command line to create the cluster using the IPs and ports of all server nodes
    # 5. Use EnOSlib Provider p.shell() to run redis-cli --cluster create in one (and only one) of the VMs

    with en.actions(roles=redis_hosts) as p:

        # copying the configuration file inside each VM
        p.copy(content=config_file.read_text(), dest="redis.conf")

        # running redis server in the background
        # We make sure to bind the service on the tansiv_address (the one who
        # uses simgrid in a TS setup)
        # We also make sure to advertise the right IP when boostraping the server
        # For example when running behind a NAT
        # or because there's some transient weirdness on Renater (which makes the
        # inter site traffic looking like being NATed):
        # https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=13448
        background_shell(p,
            "server",
            f"redis-server ./redis.conf --port {server_port} "
            "--bind {{ tansiv_address }} "
            "--cluster-announce-ip {{ tansiv_address }}")
        p.wait_for(host="{{ tansiv_address }}", port=server_port)

    # 1. create the list of redis servers: LIST: ip1:$server_port ip2:$server_port ...

    addresses = [
        f'{h.extra["tansiv_address"]}:{server_port}' for h in redis_hosts
    ]  # ["ip1:port1", "ip2:port2", ...]

    str_addresses = " ".join(addresses)  # "ip1:port1 ip2:port2 ..."

    # 2. on each node we need to launch: redis-cli --cluster create $LIST --cluster-replicas $CLUSTER_REPLICAS
    with en.actions(roles=redis_hosts[0]) as p:
        cmd = f"redis-cli --cluster create {str_addresses} --cluster-replicas {cluster_replicas} --cluster-yes"
        print(cmd)
        p.shell(cmd)


def redis_benchmark(
    bench_hosts: List[en.Host],
    redis_hosts: List[en.Host],
    redis_port: int = 7000,
    benchmark_opts: Optional[str] = "",
    working_dir: Path = Path("results").absolute(),
):

    """Run redis-benchmark for a running Redis cluster.

    The benchmark will be run from the role in bench_roles. If several roles
    are provided, only one (unspecified) will run redis-benchmark. The benchmark
    will connect to thi cluster using one (unspecified) of the roles and
    redis_port.

    Args:
        bench_hosts: singleton set of roles to run redis-benchmark from
        redis_hosts: roles running the Redis cluster nodes
        redis_port: port number to reach the cluster nodes
        benchmark_opts: additional arguments to pass to redis-benchmark (default: ""); should not include -h, -p, --csv, or --cluster
    """

    # 1. Select a single host in bench_roles and a single host in redis_roles
    # 2. Build the command line of redis-benchmark, using --csv to output in machine-readable format and storing the output in a file
    # 3. Use EnOSlib Provider p.shell() to launch redis-benchmark
    # 4. Use EnOSlib Provider p.fetch() to retrieve the output file and store it in the experiment's result directory

    remote_dir = str(time.time_ns())
    # start = time.time()

    working_dir.mkdir(parents=True, exist_ok=True)
    # (working_dir / f"redis_benchmark_{remote_dir}").write_text(str(start))

    h = redis_hosts[0]
    address = h.extra["tansiv_address"]
    with en.actions(roles=bench_hosts) as p:
        p.shell(
            f"redis-benchmark {benchmark_opts} -p {redis_port} -h {address} --csv --cluster > redis_benchmark_{remote_dir}"
        )
        p.fetch(src=f"redis_benchmark_{remote_dir}", dest=f"{working_dir}")
