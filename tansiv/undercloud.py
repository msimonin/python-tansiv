from threading import local
from typing import Dict, List, Tuple, Union

import enoslib as en
from enoslib.infra.provider import Provider


def vmong5k(clusters: Union[List[str], str], density: int, **kwargs):
    """Build a VMonG5k conf.

    Purpose, offer more flexibility on the VMs locations.
    Esp. we want to run some of our baseline on different physical hosts to
    avoid the contentions due to running all VMs on a single host (bridged
    network and share CPU).

    Args:
        clusters: a list of g5k cluster
            They will be used to store ``density`` vms each
        density: number of VM to deploy on each cluster
        kwargs: keyword argument / will be passed to .from_settings
    """
    conf = (
        en.VMonG5kConf.from_settings(job_name="tansiv", **kwargs)
    )
    for cluster in clusters:
        conf.add_machine(
            roles=["tansiv"],
            cluster=cluster,
            number=density,
            flavour_desc={
                "core": 1,
                "mem": 1024
            }
        )

    conf.finalize()

    provider = en.VMonG5k(conf)
    return provider.init()

def g5k(cluster: str, walltime: str, number: int, queue: str) -> Tuple[en.Roles, Provider]:
    prod = en.G5kNetworkConf(
        id="id",
        roles=["prod"],
        site=en.g5k_api_utils.get_cluster_site(cluster),
        type="prod",
    )
    conf = (
        en.G5kConf.from_settings(
            job_name="tansiv",
            job_type="allow_classic_ssh",
            walltime=walltime,
            queue=queue,
        )
        .add_machine(cluster=cluster, roles=["tansiv"], nodes=1, primary_network=prod)
        .add_network_conf(prod)
    ).finalize()

    provider = en.G5k(conf)
    roles, _ = provider.init()
    return roles, provider


class DummyProvider:
    def init(self):
        localhost = en.LocalHost()
        localhost.extra.update(ansible_become=True)
        return en.Roles(tansiv=[localhost]), en.Networks()

    def destroy(self):
        pass


def localhost(*args) -> Tuple[en.Roles, Provider]:
    """The undercloud is the localhost

    Use cases: when a machine is acquired by an external mean (e.g oarsub).

    Prerequisites:
        - root access must be granted beforehand (using sudo)
        - a ssh keypair must be available

    Returns
        the localhost alongside a dummy provider
        (to not break the various tricks used in the cli)
    """
    provider = DummyProvider()
    roles, _ = provider.init()
    return roles, provider


def insideout(g5k_kdict: Dict) -> Tuple[en.Roles, Provider]:
    """Experimental stuff. Factory method to get the resources.

    It's been a long time since I'd want to explore this.
    Experiments can be launched
    - as outsider: I launch my experimental code from my laptop
        The resource claim is part of the execution
    - as insider: I launch my experimental code in a job context
        The resource claim isn't part of the execution. We only need to reload
        what we got.

    When launching as an outsider, EnOSlib will use the API to claim the resource.
    When launching as an insider, we've got difference choices:
        - either reload from the job_name (default behavior in EnOSlib)
        - either reload from the job_id (also supported in EnOSlib)
    Here we choose none of the above. Since we know in advance that a single
    node is used, we only detect the context of execution (is OAR_JOB_ID set ?).
    If yes we forge our resource manually (Localhost).

    Args:
        g5k_kdict: kwargs as a dict passed to the g5k provider
    """
    import os
    job_id = os.environ.get("OAR_JOB_ID")
    if job_id is None:
        # outside
        roles, provider = g5k(**g5k_kdict)
    else:
        # inside (a g5k's node)
        roles, provider = localhost()
        from subprocess import check_call
        # enable password less access to sudo
        # in subsequent commands
        check_call("sudo-g5k")
    return roles, provider